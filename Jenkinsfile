properties([
    authorizationMatrix([
        	'hudson.model.Item.Build:performanceengineering',
            'hudson.model.Item.Cancel:performanceengineering',
            'hudson.model.Item.Discover:performanceengineering',
            'hudson.model.Item.Read:performanceengineering',
            'hudson.model.Item.ViewStatus:performanceengineering',
            'hudson.model.Item.Workspace:performanceengineering'
    ])
])

pipeline {
    agent {
        docker {
            image "docker.artifactory.yoox.net/dowcs/sretools:0.0.3"
            label "onprem"
            args "-u root"
        }
    }

    environment{ 
        git                     = credentials("bbdc")
        jumpboxkey              = credentials("jumpbox_private_key")
        aws_credentials         = credentials("${aws_account}")
        AWS_ACCESS_KEY_ID       = "${aws_credentials_USR}"
        AWS_SECRET_ACCESS_KEY   = "${aws_credentials_PSW}"
        AWS_DEFAULT_REGION      = "eu-west-1"
        ANSIBLE_FORCE_COLOR     = true
        env_type                = "${environment}".substring(0,3) 
    }

    parameters{  
        choice(name: "brand",          choices: "inseason\nton\nnap\nos3")
        string(name: "environment",    description: "e.g. int01")
        choice(name: "aws_account",    choices: "ynapinsfedev\nynapinsfeprd\nynapos3fedev\nynapos3feprd\nynapfedev\nynapfeprd\nynapnapfedev\nynapnapfeprd")
        choice(choices: 'shutdown\nstartup', description: 'State for which you want the WAS service', name: 'service_action')
    }

    options {
        ansiColor("xterm")
    }
    
    triggers { //check
        cron('0 23 * * *') 
    } 

    stages{
         stage("Prepare") {  
            steps {
               library(
                    identifier: "jenkins-shared-libraries@master",
                    retriever: modernSCM([
                        $class: "GitSCMSource",
                        credentialsId: "bbdc",
                        remote: "https://git.yoox.net/scm/dowcs/jenkins-shared-libraries.git",
                        traits: [gitBranchDiscovery()]
                    ])
                )
                script {
                    switch(env_type) {
                        case "prd":
                            domain         = "prod.e-comm"
                            env_type_basic = "prd"
                            rundeck_token  = "${rundeck_token_prd}"
                            break
                        default:
                            domain         = "dev.e-comm"
                            env_type_basic = "dev"
                            rundeck_token  = "${rundeck_token_dev}"
                    }
                }
                dir("ansible-infra") {
                    git(
                        url: "https://git.yoox.net/scm/dowcs/ansible-infra.git",
                        credentialsId: "bbdc",
                    )
                }
                dir("terraform-infra") {
                    git(
                        url: "https://git.yoox.net/scm/dowcs/terraform-infra.git",
                        credentialsId: "bbdc",
                    )
                }
                sh "cat ${jumpboxkey} > /root/.ssh/wsclnx-dev.pem && chmod 400 /root/.ssh/wsclnx-dev.pem"
                // awful workaround due to https://issues.jenkins-ci.org/browse/JENKINS-42369
                // todo: find a better way to connect to node
            }
        }  
                   

        stage('Shutdown Head') {  
            when {
                expression { params.service_action == "shutdown" }
            }
            steps {
                script {
                    parallel {
                        stage ('awsctl_servers_head_f31'){
                            sh 'cd ansible ; ansible-playbook ./awsctl_servers_head_f31.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                        }
                    }

					/*
                    setup() // setup environment

					// perform shutdown service action
                    if (CredentialsNameAWSHead != "") { 
                        withCredentials([usernamePassword(credentialsId: CredentialsNameAWSHead, passwordVariable: 'AWS_SECRET_ACCESS_KEY', usernameVariable: 'AWS_ACCESS_KEY_ID'), file(credentialsId: "wsc_ansible_vault_password", variable: "AnsibleVaultPasswordFile")]) {
                            
                            parallel (   
                                awsctl_servers_head_f31: {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_head_f31.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                }
                            )

                        }
                    } */
				}
            } 
        }

        stage('Shutdown LGs Mocks DBStats') {
            when {
                expression { params.service_action == "shutdown" }
            }
            steps {}
        }

        stage('Shutdown WCS CMS') {
            when {
                expression { params.service_action == "shutdown" }
            }
            steps{
                script {
                    /*
					//setup() // setup environment

					// perform shutdown service action
					//if (env.service_action == "shutdown" || env.service_action == "shutdown-no-dbstats") {
						
                        
                        withCredentials([usernamePassword(credentialsId: CredentialsNameAWS, passwordVariable: 'AWS_SECRET_ACCESS_KEY', usernameVariable: 'AWS_ACCESS_KEY_ID'), file(credentialsId: "wsc_ansible_vault_password", variable: "AnsibleVaultPasswordFile")]) {
                            parallel (  
                                awsctl_servers_wcs_author: {
                                    //if (service_action_wcs) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_wcs_author.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    //}
                                },
                                awsctl_servers_wcs_admin: {
                                    if (service_action_wcs) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_wcs_admin.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },											
                                awsctl_servers_wcs_index: {
                                    if (service_action_wcs) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_wcs_index.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },
                                awsctl_servers_wcs_repeater: {
                                    if (service_action_wcs) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_wcs_repeater.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },
                                awsctl_servers_wcs_search: {
                                    if (service_action_wcs) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_wcs_search.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },
                                awsctl_servers_wcs_wc: {
                                    if (service_action_wcs) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_wcs_wc.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },												
                                awsctl_servers_ogg_ext1: {
                                    if (service_action_ogg_db) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_ogg_ext1.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },												
                                awsctl_servers_cms_management: {
                                    if (service_action_cms) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_cms_management.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },
                                awsctl_servers_cms_feeder: {
                                    if (service_action_cms) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_cms_feeder.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },
                                awsctl_servers_cms_studio: {
                                    if (service_action_cms) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_cms_studio.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },
                                awsctl_servers_cms_replication1: {
                                    if (service_action_cms) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_cms_replication1.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },
                                awsctl_servers_cms_replication2: {
                                    if (service_action_cms) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_cms_replication2.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },
                                awsctl_servers_cms_cae1: {
                                    if (service_action_cms) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_cms_cae1.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },
                                awsctl_servers_cms_cae2: {
                                    if (service_action_cms) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_cms_cae2.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                },
                                awsctl_servers_cms_search: {
                                    if (service_action_cms) {
                                        sh 'cd ansible ; ansible-playbook ./awsctl_servers_cms_search.yml ${AnsibleOptions} -i inventories/ynap/hosts_coprh477.yoox.net --vault-password-file ${AnsibleVaultPasswordFile} --extra-vars \"inv_srv_num=${ynap_environment_service_number} inv_env=${ynap_environment} service_action=${service_action}\" '
                                    }
                                }
                            )
                        } 
						
					//} */
				}
            }
        }
        stage('Shutdown WXS MQ Database') {
            when {
                expression { params.service_action == "shutdown" }
            }
            steps{}
        }



        stage('Startup Database WXS MQ') {
            when {
                expression { params.service_action == "startup" }
            }
            steps{}
        }
        stage('Startup WCS-index CMS-mgmt') {
            when {
                expression { params.service_action == "startup" }
            }
            steps{}
        }
        stage('Startup WCS-repeater CMS-replication') {
            when {
                expression { params.service_action == "startup"}
            }
            steps{}
        }
        stage('Startup WCS-Author WCS-live') {
            when {
                expression { params.service_action == "startup" }
            }
            steps{}
        }
        stage('Startup CMS-Studio CMS-Live LGs Mocks') {
            when {
                expression { params.service_action == "startup" }
            }
            steps{}
        }
        stage('Startup Head') {
            when {
                expression { params.service_action == "startup" }
            }
            steps{}
        }
    }

    post {
         success {
            slackSend(
                channel: "sre-wcs-pipeline-reports", 
                message: "Success - ${JOB_NAME} \n ${BUILD_URL}",
                color:   "#00ff00"
            )
        }
        unsuccessful {
            slackSend(
                channel: "sre-wcs-pipeline-reports,sre-wcs-pipeline-failures",
                message: "Failure - ${JOB_NAME} \n ${BUILD_URL}",
                color:   "#ff0000"
            )
        } 
        always { //clean up task need to be added
            cleanWs()
        }
    }
}